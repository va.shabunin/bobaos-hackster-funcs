const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions

const MY_DEV_TOKEN = 'INTERT FCM DEVICE TOKEN HERE';

exports.handleCriticalTemp = functions.pubsub
  .topic('critical-temp-topic')
  .onPublish(message => {
    const messageBody = message.data
      ? Buffer.from(message.data, 'base64').toString()
      : null;
    const data = JSON.parse(messageBody);
    console.log(JSON.stringify(data));
    const current = data.current;
    const low = data.low;
    const high = data.high;
    const atom_id = data.atom_id;
    if (current > high) {
      console.log('current > critical. sending notification');
      const payload = {
        notification: {
          title: `Temp alarm`,
          body: `Temp > critical. Current ${current}, critical: ${high}. Atom: ${atom_id}.`
        }
      };

      // Send notifications to device token.
      return admin.messaging().sendToDevice(MY_DEV_TOKEN, payload);
    }
    if (current < low) {
      console.log('current < critical. sending notification');
      const payload = {
        notification: {
          title: `Temp alarm`,
          body: `Temp < critical. Current ${current}, critical: ${low}. Atom: ${atom_id}.`
        }
      };

      // Send notifications to device token.
      return admin.messaging().sendToDevice(MY_DEV_TOKEN, payload);
    }

    return Promise.resolve();
  });
