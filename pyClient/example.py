import os
import json
from bdsd_async import BDSDClient
from helium_client import Helium


helium = Helium("/dev/ttyS1")
helium.connect()

# init channel
channel = helium.create_channel('iot0x03')
config = channel.config()

low = config.get('config.low_temp')
high = config.get('config.high_temp')
atom_id = config.get('config.atom_id')
# init bobaos client
SOCKFILE = os.environ.copy()['XDG_RUNTIME_DIR'] + '/bdsd.sock'
myClient = BDSDClient(SOCKFILE)


@myClient.on('connected')
def handle_connected():
    print('myClient is connected, great then')

@myClient.on('value')
def handle_broadcasted_value(data):
    # now we are watching datapoitnt with number 1. it's temperature
    datapoint = data['id']
    if datapoint == 1:
        current = data['value']
        # get critical temp from config
        publishedData = {'current': current, 'low': low, 'high': high, 'datapoint': datapoint, 'atom_id': atom_id}
        # publish data to IoT Cloud. { current: 24.5, high: 30, low: 20, datapoint: 1, atom_id: '112233445566aabb'}
	print('sending..', json.dumps(publishedData))
        channel.send(json.dumps(publishedData))

myClient.loop()
